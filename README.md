# serverspec

## Pré-requisitos

Instale as ferramentas de apoio aos testes

- [Vagrant](https://www.vagrantup.com/)
- [VirtualBox](http://www.virtualbox.org/)

Instale as gems abaixo

    $ gem install serverspec highline net

## Como usar este exemplo?

1. Clone este projeto

    ```
    git clone https://gitlab.com/meetup-puppet-br/example-serverspec.git
    ```

1. Clone o projeto de exemplo de módulo ([example-module](https://gitlab.com/meetup-puppet-br/example-module)) como módulo deste projeto
    ```
    git clone https://gitlab.com/meetup-puppet-br/example-module.git example-serverspec/modules/apache
    ```

1. Alterne entre as *branches* para ver os resultados de cada `demo` (exemplo)

  Após alternar entre as *branches* execute:

  - `rake`: para realizar as tarefas de testes (e observar os resultados)
  - `vagrant provision`: para realizar o provisionamento da máquina segundo a etapa de demonstração  

  *Branches* de demonstração
  1. *branch* `demo`: testes básicos (reportam-se erros)
  1. *branch* `demo_bdd`: testes expandidos com blocos/*osfamily* (reportam-se erros)
  1. *branch* `demo_bdd_solved`: testes têm soluções resolvidas

## serverspec: Instruções base de setup inicial

  ***(não necessários se para usar neste projeto exemplo)***

Inicie a estrutura de testes

    $ mkdir -p test/bdd/example
    $ mkdir -p test/bdd/example/modules
    $ mkdir -p test/bdd/example/environments/dev
    $ cd test/bdd/example

Crie Vagrantfile

    $ vagrant init -m

Ajuste o Vagrantfile para os testes

```
# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"
ENVIRONMENT = 'dev'

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = "puppetlabs/centos-7.0-64-puppet"
  config.vm.box_check_update = false

  config.vm.define "example-serverspec" do |server|
    server.vm.provider "virtualbox" do |vbox|
      vbox.customize [ "modifyvm", :id, "--cpus", "1" ]
      vbox.customize [ "modifyvm", :id, "--memory", "2048" ]
    end
  end

  config.vm.provision "node", type: "puppet", :options => ["--pluginsync"] do |puppet|
    puppet.module_path = "modules"
    puppet.manifest_file = "site.pp"
    puppet.options = "--environment #{ENVIRONMENT} --verbose "
    puppet.environment = "#{ENVIRONMENT}"
    puppet.environment_path = "environments"
    puppet.manifests_path = "manifests"
  end

end
```

Execute o `serverspec` para inicializar configuração padrão

    $ serverspec-init

Responda às perguntas conforme os testes que irá realizar.

1. Sistema operacional:

  ```
  Select OS type:

    1) UN*X
    2) Windows

  Select number:
  ```

1. Forma de execução do comandos do `serverspec`:

  ```
  Select a backend type:

    1) SSH
    2) Exec (local)

  Select number:
  ```

1. Responda `y` para testes usando Vagrant:

  ```
  Vagrant instance y/n:
  ```

1. Responda `y` para usar dados do Vagrantfile criado anteriormente:

  ```
  Auto-configure Vagrant from Vagrantfile? y/n:
  ```
